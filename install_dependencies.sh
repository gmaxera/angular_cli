#!/usr/bin/env bash

apt-get update

apt-get install -y nodejs git

npm install -g typescript

npm install -g @angular/cli
