# Version: 0.1
## Each instruction commit a layer to the image

FROM debian:stretch

MAINTAINER Gianluca Massera "gmaxera@gmail.com"

SHELL ["/bin/bash", "-c"]

# IMPORTANT !!
# In order to have a shared external_sources across docker building
# the context of the building the the root of the repository
# not the directory where Dockerfile resides

COPY ./install_*.sh /root/

RUN apt-get update \
	&& apt-get -y install gnupg2 \
	&& /root/install_nodejs.sh \
	&& /root/install_dependencies.sh \
	&& rm -rf /var/lib/apt/lists/*

COPY start_command.sh /usr/bin/

VOLUME /var/www

EXPOSE 4200

ENTRYPOINT ["/usr/bin/start_command.sh"]
CMD ["ng serve --host 0.0.0.0 --disable-host-check"]

## HEALTHCHECK command allow to define how to check everything is working as expected
